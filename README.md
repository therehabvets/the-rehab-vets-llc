The Rehab Vets provide veterinary rehabilitation services including laser and shockwave therapies and acupuncture. Mobile and clinical locations available.

Veterinary co-op for other healthcare professionals to use our space and equipment.

Address: 20 W Dry Creek Circle, Suite 230, Littleton, CO 80120, USA

Phone: 303-913-0876

Website: https://www.therehabvets.com
